from twilio.rest import Client
import json

with open("secrets.json", "r") as secrets:
    secret = json.load(secrets)
    twilio_secrets = secret["twilio_secrets"]
    sms_details = secret["sms_details"]


client = Client(twilio_secrets["sid_account"], twilio_secrets["auth_token"])
message = client.messages.create(body=sms_details["body"], 
                                from_=sms_details["from"], 
                                to=sms_details["to"] )

print(message.sid)